import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  constructor(
    public apis: ApiService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {}

  public doLogout() {
    this.spinner.show();
    this.apis
      .post("/api/v1/admin/logout", {
        token: this.cookies.get("adminToken")
      })
      .subscribe(
        (res: Response) => {
          this.spinner.hide();
          if (res["status"].toString() == "success") {
            /*  this.cookies.deleteAll();
          localStorage.clear(); */
            //this.cookies.delete("name");
            this.cookies.delete("adminToken");
            this.apis.adminLogged = "";
            //this.apis.loggedIn = "";
            this.toast.success("Logged Out");
            this.router.navigate(["/"]);
            //this.cookies.delete("role");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          this.spinner.hide();
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
