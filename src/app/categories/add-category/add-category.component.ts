import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: "app-add-category",
  templateUrl: "./add-category.component.html",
  styleUrls: ["./add-category.component.css"]
})
export class AddCategoryComponent implements OnInit {
  subCatArray = [];
  newSubCat: string = "";
  name: string = "";
  commission: number;
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {}

  deleteSubCat(i) {
    this.subCatArray.splice(i, 1);
  }

  addSubCat() {
    if (this.newSubCat != "") {
      this.subCatArray.push(this.newSubCat);
      this.newSubCat = "";
      console.log("this.subCatArray");
      console.log(this.subCatArray);
    }
  }

  addCategory() {
    if (this.subCatArray.length != 0) {
      this.apis
        .post("/api/v1/admin/addCategory/", {
          token: this.cookies.get("adminToken"),
          name: this.name,
          sub_cat: this.subCatArray,
          commission: this.commission
        })
        .subscribe(
          (res: Response) => {
            console.log("addCat");
            console.log(res);
            if (res["status"].toString() == "success") {
              this.toast.success("Added Successfully");
              this.router.navigate(["/categories"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (res: Response) => {
            console.log(res);
            // if (res["error"].message == "Errors") {
            //   this.toast.error(res["error"].data.errors[0].msg);
            // } else {
            //   this.toast.error(res["error"].message);
            // }
            this.toast.error("Authentication Failed");
          }
        );
    } else {
      this.toast.error("Please Add Subcategory");
    }
  }
}
