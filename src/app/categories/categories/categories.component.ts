import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: "app-categories",
  templateUrl: "./categories.component.html",
  styleUrls: ["./categories.component.css"]
})
export class CategoriesComponent implements OnInit {
  allCat = [];
  subCatArray = [];
  newSubCat: string = "";
  name: string = "";
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.apis.get("/api/v1/admin/getCategories").subscribe(
      (res: Response) => {
        // console.log("AllCategories");
        // console.log(res);
        if (res["status"].toString() == "success") {
          for (var i = 0; i < res["data"].length; i++) {
            if (res["data"][i].subcategories[0]) {
              res["data"][i].subcategories = res["data"][
                i
              ].subcategories[0].split(",");
            }
          }

          this.allCat = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        // if (res["error"].message == "Errors") {
        //   this.toast.error(res["error"].data.errors[0].msg);
        // } else {
        //   this.toast.error(res["error"].message);
        // }
      }
    );
  }

  deleteCategory(id) {
    this.apis
      .post("/api/v1/admin/deleteCategory", {
        cat_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Deleted");
            this.getCategories();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
