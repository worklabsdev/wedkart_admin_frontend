import { NgModule, Component } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { BuyerSellerComponent } from "./buyer-seller/buyer-seller/buyer-seller.component";
import { EditBuyerSellerComponent } from "./buyer-seller/edit-buyer-seller/edit-buyer-seller.component";
import { EditCategoryComponent } from "./categories/edit-category/edit-category.component";
import { CategoriesComponent } from "./categories/categories/categories.component";
import { ProductsComponent } from "./products/products/products.component";
import { EditProductComponent } from "./products/edit-product/edit-product.component";
import { ShopsComponent } from "./shops/shops/shops.component";
import { EditShopComponent } from "./shops/edit-shop/edit-shop.component";
import { OrdersComponent } from "./orders/orders/orders.component";
import { ViewOrderComponent } from "./orders/view-order/view-order.component";
import { CancellationRequestComponent } from "./orders/cancellation-request/cancellation-request.component";
import { HomeComponent } from "./dashboard/home/home.component";
import { FundsComponent } from "./dashboard/funds/funds.component";
import { LoginComponent } from "./login/login.component";
import { ProfileComponent } from "./dashboard/profile/profile.component";
import { SubCategoriesComponent } from "./sub-categories/sub-categories/sub-categories.component";
import { EditSubCategoryComponent } from "./sub-categories/edit-sub-category/edit-sub-category.component";
import { SellerComponent } from "./buyer-seller/seller/seller.component";
import { EditSellerComponent } from "./buyer-seller/edit-seller/edit-seller.component";
import { ViewSellerComponent } from "./buyer-seller/view-seller/view-seller.component";
import { ViewBuyerSellerComponent } from "./buyer-seller/view-buyer-seller/view-buyer-seller.component";
import { AddCategoryComponent } from "./categories/add-category/add-category.component";

const appRoutes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "dashboard",
    component: HomeComponent
  },
  {
    path: "profile",
    component: ProfileComponent
  },
  {
    path: "funds",
    component: FundsComponent
  },
  {
    path: "categories",
    component: CategoriesComponent
  },
  {
    path: "add-category",
    component: AddCategoryComponent
  },
  {
    path: "category/:id",
    component: EditCategoryComponent
  },
  {
    path: "buyers",
    component: BuyerSellerComponent
  },
  {
    path: "edit-buyer-seller/:id",
    component: EditBuyerSellerComponent
  },
  {
    path: "buyer/:id",
    component: ViewBuyerSellerComponent
  },
  {
    path: "orders",
    component: OrdersComponent
  },
  {
    path: "view-order/:id",
    component: ViewOrderComponent
  },
  {
    path: "cancellation-request",
    component: CancellationRequestComponent
  },
  {
    path: "products",
    component: ProductsComponent
  },
  {
    path: "edit-product/:id",
    component: EditProductComponent
  },
  {
    path: "shops",
    component: ShopsComponent
  },
  {
    path: "edit-shop/:id",
    component: EditShopComponent
  },
  {
    path: "subcategories",
    component: SubCategoriesComponent
  },
  {
    path: "edit-subcategory/:id",
    component: EditSubCategoryComponent
  },
  {
    path: "sellers",
    component: SellerComponent
  },
  {
    path: "seller/:id",
    component: ViewSellerComponent
  },
  {
    path: "edit-seller/:id",
    component: EditSellerComponent
  }
];
@NgModule({
  imports: [CommonModule, RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutes {}
