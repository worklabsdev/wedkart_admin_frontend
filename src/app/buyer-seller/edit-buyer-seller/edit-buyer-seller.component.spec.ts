import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBuyerSellerComponent } from './edit-buyer-seller.component';

describe('EditBuyerSellerComponent', () => {
  let component: EditBuyerSellerComponent;
  let fixture: ComponentFixture<EditBuyerSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBuyerSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBuyerSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
