import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { ApiService } from "../../api.service";
import { CookieService } from "ngx-cookie-service";
import "rxjs";
import { map } from "rxjs/operators";
import { DomSanitizer } from "@angular/platform-browser";
declare var $: any;
@Component({
  selector: "app-seller",
  templateUrl: "./seller.component.html",
  styleUrls: ["./seller.component.css"]
})
export class SellerComponent implements OnInit {
  public allSellers = [];
  public showSellers: any = "";
  public currPage: number = 1;
  public pageNum: number = 1;
  public seller = [];
  name: string = "";
  phone: string = "";
  email: string = "";
  aname: string = "";
  aphone: string = "";
  aemail: string = "";
  constructor(
    private apis: ApiService,
    private toast: ToastrService,
    /*private spinner: NgxSpinnerService, */
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAllSellers();
  }

  previous() {
    if (this.currPage == 1) {
      this.toast.error("You Are Already on the 1st Page");
    } else {
      this.currPage -= 1;
      this.getAllSellers();
    }
  }

  next() {
    this.currPage += 1;
    this.getAllSellers();
  }

  getAllSellers() {
    this.apis
      .post("/api/v1/admin/sellers", {
        token: this.cookies.get("adminToken"),
        page: this.currPage,
        filter: "seller"
      })

      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["message"] == "No User Found") {
            this.toast.error("No more users asre there");
          }
          if (res["status"].toString() == "success") {
            this.allSellers = res["data"]["list"];
            if (this.allSellers.length != 0) {
              this.showSellers = 1;
            } else {
              this.showSellers = "";
            }
            //this.currPage = res["data"].currPage;
            this.pageNum = this.currPage;
            console.log("allSellers", this.allSellers);
            console.log("currPage", this.currPage);
          }
          // else {
          //   this.toast.error("Some Problem Occured");
          // }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  deleteSeller(id) {
    this.apis
      .post("/api/v1/admin/seller/delete", {
        token: this.cookies.get("adminToken"),
        _id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Deleted");
            this.getAllSellers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }
  disableSeller(id) {
    console.log(id);
    this.apis
      .post("/api/v1/admin/seller/inactivate", {
        token: this.cookies.get("adminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Blocked");
            this.getAllSellers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  enableSeller(id) {
    this.apis
      .post("/api/v1/admin/seller/activate", {
        token: this.cookies.get("adminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Unblocked");
            this.getAllSellers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  getSellerDetails(id) {
    this.apis
      .post("/api/v1/admin/seller", {
        token: this.cookies.get("adminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log("sellerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.seller = res["data"];
            this.name = this.seller["name"];
            this.email = this.seller["email"];
            this.phone = this.seller["phone"];
            console.log(this.seller);
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }

  updateSellerDetails() {
    this.apis
      .post("/api/v1/admin/seller/update", {
        token: this.cookies.get("adminToken"),
        _id: this.seller["_id"],
        name: this.name,
        email: this.email,
        phone: this.phone
      })
      .subscribe(
        (res: Response) => {
          console.log("sellerUpdate");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Updated");
            this.getAllSellers();
            this.name = "";
            this.email = "";
            this.phone = "";
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }

  addNewSeller() {
    this.apis
      .post("/api/v1/admin/seller/add", {
        token: this.cookies.get("adminToken"),
        name: this.aname,
        email: this.aemail,
        phone: this.aphone
      })
      .subscribe(
        (res: Response) => {
          console.log("sellerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Added");
            this.getAllSellers();
            this.aname = "";
            this.aemail = "";
            this.aphone = "";
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }
}
