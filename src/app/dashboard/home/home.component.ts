import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  latestOrders = [];
  buyers: number = 0;
  sellers: number = 0;
  orders: any = "";
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.getLatestOrders();
    this.getBuyers();
    this.getSellers();
  }

  getLatestOrders() {
    this.apis.get("/api/v1/admin/latestOrders").subscribe(
      (res: Response) => {
        if (res["data"].length != 0) {
          this.latestOrders = res["data"];
          this.orders = 1;
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
  getBuyers() {
    this.apis.get("/api/v1/admin/buyers").subscribe(
      (res: Response) => {
        this.buyers = res["data"];
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
  getSellers() {
    this.apis.get("/api/v1/admin/sellers").subscribe(
      (res: Response) => {
        this.sellers = res["data"];
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
}

