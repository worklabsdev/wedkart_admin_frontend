import { Component } from "@angular/core";
import { Router } from "@angular/router";
// import { ToastrService } from "ngx-toastr";
// import { ApiService } from "./api.service";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "WeddingKart-admin";
  constructor(
    // private apis: ApiService,
    // private toast: ToastrService,
    /*private spinner: NgxSpinnerService, */
    private cookies: CookieService,
    private router: Router
  ) {}
  ngOnInit() {
    if (!this.cookies.get("adminToken")) {
      this.router.navigate(["/"]);
    }
  }
}
